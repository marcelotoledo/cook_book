# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

cuisine = Cuisine.create(name: 'Árabe')
recipe_type = RecipeType.create(name: 'Prato Principal')

Recipe.create(
  title: 'Bolo de cenoura',
  recipe_type: recipe_type,
  cuisine: cuisine,
  difficulty: 'Medio',
  cook_time: 60,
  ingredients: 'cenoura, ovos, farinha de trigo e leite',
  method: 'Bata tudo no liquidificador e leve ao forno'
)
