require 'rails_helper'

feature 'User cant edit recipe type' do
  scenario 'accesses the page correctly' do
    recipe_type = RecipeType.create(name: 'Prato principal')

    visit root_path
    click_on recipe_type.name
    click_on 'Editar'

    expect(page).to have_css('h1', text: 'Edição de tipo de receita')
    expect(page).to have_content('Voltar')
  end

  scenario 'successfully' do
    recipe_type = RecipeType.create(name: 'Prato principal')

    visit edit_recipe_type_path recipe_type
    fill_in 'Nome', with: 'Prato de Entrada'
    click_on 'Editar'

    expect(current_path).to eq recipe_type_path(1)
    expect(page).to have_css('h1', text: 'Prato de Entrada')
  end

  scenario 'cant be blank' do
    recipe_type = RecipeType.create(name:  'Prato principal')

    visit edit_recipe_type_path recipe_type
    fill_in 'Nome', with: ''
    click_on 'Editar'

    expect(page).to have_content('Você deve preencher o nome do tipo de receita')
  end
end
