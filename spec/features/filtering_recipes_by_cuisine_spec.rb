require 'rails_helper'

feature 'Visitor view recipes by cuisine' do
  scenario 'from home page' do
    cuisine = Cuisine.create(name: 'Brasileira')
    recipe_type = RecipeType.create(name: 'Sobremesa')
    recipe = Recipe.create params(cuisine, recipe_type)

    visit root_path
    click_on cuisine.name

    expect(page).to have_css('h1', text: cuisine.name)
    expect(page).to have_css('h1', text: recipe.title)
    expect(page).to have_css('h3', text: 'Detalhes')
    expect(page).to have_css('li', text: recipe.recipe_type.name)
    expect(page).to have_css('li', text: recipe.cuisine.name)
    expect(page).to have_css('li', text: recipe.difficulty)
    expect(page).to have_css('li', text: "#{recipe.cook_time} minutos")
    expect(page).to have_css('h3', text: 'Ingredientes')
    expect(page).to have_css('li', text: recipe.ingredients)
    expect(page).to have_css('h3', text: 'Como Preparar')
    expect(page).to have_css('li', text: recipe.method)
  end

  scenario 'and view only cuisine recipes' do
    brazilian_cuisine = Cuisine.create(name: 'Brasileira')
    brazilian_recipe_type = RecipeType.create(name: 'Sobremesa')
    brazilian_recipe = Recipe.create params(brazilian_cuisine, brazilian_recipe_type)
    italian_cuisine = Cuisine.create(name: 'Italiana')
    italian_recipe_type = RecipeType.create(name: 'Prato Principal')
    italian_recipe = Recipe.create other_params(italian_cuisine, italian_recipe_type)

    visit root_path
    click_on italian_cuisine.name

    expect(page).to have_css('h1', text: italian_cuisine.name)
    expect(page).to have_css('h1', text: italian_recipe.title)
    expect(page).to have_css('h3', text: 'Detalhes')
    expect(page).to have_css('li', text: italian_recipe.recipe_type.name)
    expect(page).to have_css('li', text: italian_recipe.cuisine.name)
    expect(page).to have_css('li', text: italian_recipe.difficulty)
    expect(page).to have_css('li', text: "#{italian_recipe.cook_time} minutos")
    expect(page).to have_css('h3', text: 'Ingredientes')
    expect(page).to have_css('li', text: italian_recipe.ingredients)
    expect(page).to have_css('h3', text: 'Como Preparar')
    expect(page).to have_css('li', text: italian_recipe.method)
    expect(page).not_to have_content(brazilian_cuisine.name)
  end

  scenario 'and cuisine has no recipe' do
    cuisine = Cuisine.create(name: 'Tailandesa')
    brazilian_cuisine = Cuisine.create(name: 'Brasileira')
    brazilian_recipe_type = RecipeType.create(name: 'Sobremesa')
    brazilian_recipe = Recipe.create params(brazilian_cuisine, brazilian_recipe_type)

    visit cuisine_path(cuisine)

    expect(page).not_to have_content(brazilian_recipe.title)
    expect(page).to have_content('Nenhuma receita encontrada para este tipo de cozinha')
  end
end

private

  def params(cuisine, recipe_type)
    {
      title: 'Bolo de cenoura', recipe_type: recipe_type, cuisine: cuisine,
      difficulty: 'Medio', cook_time: 60,
      ingredients: 'cenoura, ovos, farinha de trigo e leite',
      method: 'Bata tudo no liquidificador e leve ao forno'
    }
  end

  def other_params(cuisine, recipe_type)
    {
      title: 'Macarrão Carbonara', recipe_type: recipe_type, cuisine: cuisine,
      difficulty: 'Difícil', cook_time: 30,
      ingredients: 'Massa, ovos, bacon',
      method: 'Frite o bacon; Cozinhe a massa ate ficar al dent; Misture os ovos e o bacon a massa ainda quente;'
    }
  end
