require 'rails_helper'

feature 'User can edites recipes' do
  scenario 'can go to edit page' do
    cuisine = Cuisine.create(name: 'Brasileira')
    recipe_type = RecipeType.create(name: 'Sobremesa')
    recipe = Recipe.create params(cuisine, recipe_type)

    visit recipe_path recipe
    click_on 'Editar'

    expect(current_path).to eq edit_recipe_path recipe
    expect(page).to have_css('h1', text: 'Edição de receitas')
  end

  scenario 'successfully' do
    cuisine = Cuisine.create(name: 'Brasileira')
    recipe_type = RecipeType.create(name: 'Sobremesa')
    new_recipe_type = RecipeType.create(name: 'Prato Principal')
    recipe = Recipe.create params(cuisine, recipe_type)

    visit edit_recipe_path recipe

    fill_in 'Título', with: 'Creme de cenoura'
    select new_recipe_type.name, from: 'Tipo da Receita'
    fill_in 'Dificuldade', with: 'Fácil'
    fill_in 'Tempo de Preparo', with: 40
    fill_in 'Ingredientes', with: 'Cenoura, ovos, leite condensado'
    click_on 'Editar'
    recipe.reload

    expect(current_path).to eq recipe_path 1
    expect(page).to have_css('h1', text: 'Creme de cenoura')
    expect(page).to have_css('h3', text: 'Detalhes')
    expect(page).to have_css('li', text: new_recipe_type.name)
    expect(page).to have_css('li', text: cuisine.name)
    expect(page).to have_css('li', text: 'Fácil')
    expect(page).to have_css('li', text: "40 minutos")
    expect(page).to have_css('h3', text: 'Ingredientes')
    expect(page).to have_css('li', text: recipe.ingredients)
    expect(page).to have_css('h3', text: 'Como Preparar')
    expect(page).to have_css('li', text: recipe.method)
  end

  scenario 'cant be blank' do
    recipe = create(:recipe)

    visit edit_recipe_path recipe
    fill_in 'Título', with: ''
    fill_in 'Dificuldade', with: ''
    fill_in 'Tempo de Preparo', with: ''
    fill_in 'Ingredientes', with: ''
    click_on 'Editar'

    expect(page).to have_content('Você deve preencher todos os dados da receita')
  end
end

private

  def params(cuisine, recipe_type)
    {
      title: 'Bolo de cenoura', recipe_type: recipe_type, cuisine: cuisine,
      difficulty: 'Medio', cook_time: 60,
      ingredients: 'cenoura, ovos, farinha de trigo e leite',
      method: 'Bata tudo no liquidificador e leve ao forno'
    }
  end
