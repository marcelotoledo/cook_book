require 'rails_helper'

feature 'Visitor visit homepage' do
  scenario 'successfully' do
    welcome = 'Bem-vindo ao maior livro de receitas online'

    visit root_path

    expect(page).to have_css('h1', text: 'CookBook')
    expect(page).to have_css('p', text: welcome)
  end

  scenario 'and view recipes list' do
    cuisine = Cuisine.create(name: 'Brasileira')
    recipe_type = RecipeType.create(name: 'Sobremesa')
    another_recipe_type = RecipeType.create(name: 'Prato Principal')
    recipe = Recipe.create(params(cuisine, recipe_type))
    another_recipe = Recipe.create(new_params(cuisine, another_recipe_type))

    visit root_path

    expect(page).to have_css('h1', text: recipe.title)
    expect(page).to have_css('li', text: recipe.recipe_type.name)
    expect(page).to have_css('li', text: recipe.cuisine.name)
    expect(page).to have_css('li', text: recipe.difficulty)
    expect(page).to have_css('li', text: "#{recipe.cook_time} minutos")

    expect(page).to have_css('h1', text: another_recipe.title)
    expect(page).to have_css('li', text: another_recipe.recipe_type.name)
    expect(page).to have_css('li', text: another_recipe.cuisine.name)
    expect(page).to have_css('li', text: another_recipe.difficulty)
    expect(page).to have_css('li', text: "#{another_recipe.cook_time} minutos")
  end
end

private

  def params(cuisine, recipe_type)
    {
      title: 'Bolo de cenoura', recipe_type: recipe_type,
      cuisine: cuisine, difficulty: 'Médio', cook_time: 60,
      ingredients: 'cenoura, ovos, farinha de trigo e leite',
      method: 'Bata tudo no liquidificador e leve ao forno'
    }
  end

  def new_params(cuisine, recipe_type)
    {
      title: 'Feijoada', recipe_type: recipe_type,
      cuisine: cuisine, difficulty: 'Difícil', cook_time: 90,
      ingredients: 'Feijao, paio, carne seca',
      method: 'Cozinhar o feijao e refogar com as carnes já preparadas',
    }
  end
