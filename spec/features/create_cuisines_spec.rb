require 'rails_helper'

feature 'Create cuisines' do
  scenario 'accesses the page correctly' do
    visit root_path
    click_on 'Cadastrar cozinha'

    expect(current_path).to eq new_cuisine_path
    expect(page).to have_css('h1', text: 'Cadastro de cozinhas')
    expect(page).to have_content('Voltar')
  end

  scenario "and can't be blank" do
    visit new_cuisine_path
    click_on 'Enviar'

    expect(page).to have_content('Você deve preencher o nome da cozinha')
  end

  scenario 'successfully' do
    visit new_cuisine_path
    fill_in 'Nome', with: 'Chinesa'
    click_on 'Enviar'

    expect(page).to have_css('h1', text: 'Chinesa')
    expect(page).to have_content('Nenhuma receita encontrada para este tipo de cozinha')
  end
end
