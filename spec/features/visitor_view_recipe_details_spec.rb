require 'rails_helper'

feature 'Visitor view recipe details' do
  scenario 'successfully' do
    cuisine = Cuisine.create(name: 'Brasileira')
    recipe_type = RecipeType.create(name: 'Sobremesa')
    recipe = Recipe.create params(cuisine, recipe_type)

    visit root_path
    click_on 'Bolo de cenoura'

    expect(page).to have_css('h1', text: recipe.title)
    expect(page).to have_css('h3', text: 'Detalhes')
    expect(page).to have_css('li', text: recipe.recipe_type.name)
    expect(page).to have_css('li', text: recipe.cuisine.name)
    expect(page).to have_css('li', text: recipe.difficulty)
    expect(page).to have_css('li', text: "#{recipe.cook_time} minutos")
    expect(page).to have_css('h3', text: 'Ingredientes')
    expect(page).to have_css('li', text: recipe.ingredients)
    expect(page).to have_css('h3', text: 'Como Preparar')
    expect(page).to have_css('li', text: recipe.method)
  end

  scenario 'and return to recipe list' do
    cuisine = Cuisine.create(name: 'Brasileira')
    recipe_type = RecipeType.create(name: 'Sobremesa')
    recipe = Recipe.create params(cuisine, recipe_type)

    visit root_path
    click_on recipe.title
    click_on 'Voltar'

    expect(current_path).to eq root_path
  end
end

private

  def params(cuisine, recipe_type)
    {
      title: 'Bolo de cenoura', recipe_type: recipe_type, cuisine: cuisine,
      difficulty: 'Medio', cook_time: 60,
      ingredients: 'cenoura, ovos, farinha de trigo e leite',
      method: 'Bata tudo no liquidificador e leve ao forno'
    }
  end
