require 'rails_helper'

feature 'User filter recipes by recipe type' do
  scenario 'from home page' do
    cuisine = Cuisine.create(name: 'Brasileira')
    recipe_type = RecipeType.create(name: 'Sobremesa')
    recipe = Recipe.create params(cuisine, recipe_type)

    visit root_path
    click_on recipe_type.name

    expect(page).to have_css('h1', text: recipe_type.name)
    expect(page).to have_css('h1', text: recipe.title)
    expect(page).to have_css('li', text: recipe.recipe_type.name)
    expect(page).to have_css('li', text: recipe.cuisine.name)
    expect(page).to have_css('li', text: recipe.difficulty)
    expect(page).to have_css('li', text: "#{recipe.cook_time} minutos")
  end

  scenario 'and view only cuisine recipes' do
    brazilian_cuisine = Cuisine.create(name: 'Brasileira')
    dessert_recipe_type = RecipeType.create(name: 'Sobremesa')
    dessert_recipe = Recipe.create params(brazilian_cuisine, dessert_recipe_type)

    italian_cuisine = Cuisine.create(name: 'Italiana')
    main_recipe_type = RecipeType.create(name: 'Prato Principal')
    main_recipe = Recipe.create other_params(italian_cuisine, main_recipe_type)

    visit root_path
    click_on main_recipe_type.name

    expect(page).to have_css('h1', text: main_recipe.title)
    expect(page).to have_css('li', text: main_recipe.recipe_type.name)
    expect(page).to have_css('li', text: main_recipe.cuisine.name)
    expect(page).to have_css('li', text: main_recipe.difficulty)
    expect(page).to have_css('li', text: "#{main_recipe.cook_time} minutos")
    expect(page).not_to have_css('h1', text: dessert_recipe.title)
    expect(page).not_to have_css('li', text: dessert_recipe.recipe_type.name)
    expect(page).not_to have_css('li', text: dessert_recipe.cuisine.name)
    expect(page).not_to have_css('li', text: dessert_recipe.difficulty)
    expect(page).not_to have_css('li', text: "#{dessert_recipe.cook_time} minutos")
  end

  scenario 'and cuisine has no recipe' do
    cuisine = Cuisine.create(name: 'Brasileira')
    recipe_type = RecipeType.create(name: 'Sobremesa')
    recipe = Recipe.create params(cuisine, recipe_type)
    main_dish_type = RecipeType.create(name: 'Prato Principal')

    visit root_path
    click_on main_dish_type.name

    expect(page).not_to have_content(recipe.title)
    expect(page).to have_content('Nenhuma receita encontrada para este tipo de receita')
  end
end

private

  def params(cuisine, recipe_type)
    {
      title: 'Bolo de cenoura', recipe_type: recipe_type, cuisine: cuisine,
      difficulty: 'Medio', cook_time: 60,
      ingredients: 'cenoura, ovos, farinha de trigo e leite',
      method: 'Bata tudo no liquidificador e leve ao forno'
    }
  end

  def other_params(cuisine, recipe_type)
    {
      title: 'Macarrão Carbonara', recipe_type: recipe_type, cuisine: cuisine,
      difficulty: 'Difícil', cook_time: 30,
      ingredients: 'Massa, ovos, bacon',
      method: 'Frite o bacon; Cozinhe a massa ate ficar al dent; Misture os ovos e o bacon a massa ainda quente;'
    }
  end
