require 'rails_helper'

feature 'Create recipes' do
  scenario 'accesses the page correctly' do
    visit root_path
    click_on 'Enviar uma receita'

    expect(current_path).to eq new_recipe_path
    expect(page).to have_css('h1', text: 'Cadastro de receitas')
    expect(page).to have_content('Voltar')
  end

  scenario 'successfully' do
    cuisine = Cuisine.create(name: 'Árabe')
    recipe_type = RecipeType.create(name: 'Prato Principal')
    RecipeType.create(name: 'Entrada')
    RecipeType.create(name: 'Sobremesa')
    ingredients = 'Trigo para quibe, cebola, tomate picado, azeite, salsinha'
    method = 'Misturar tudo e servir. Adicione limão a gosto.'

    visit new_recipe_path

    fill_in 'Título', with: 'Tabule'
    select recipe_type.name, from: 'Tipo da Receita'
    select cuisine.name, from: 'Cozinha'
    fill_in 'Dificuldade', with: 'Fácil'
    fill_in 'Tempo de Preparo', with: '45'
    fill_in 'Ingredientes', with: ingredients
    fill_in 'Como Preparar', with: method
    click_on 'Enviar'

    expect(current_path).to eq recipe_path 1
    expect(page).to have_css('h1', text: 'Tabule')
    expect(page).to have_css('h3', text: 'Detalhes')
    expect(page).to have_css('li', text: recipe_type.name)
    expect(page).to have_css('li', text: cuisine.name)
    expect(page).to have_css('li', text: 'Fácil')
    expect(page).to have_css('li', text: "45 minutos")
    expect(page).to have_css('h3', text: 'Ingredientes')
    expect(page).to have_css('li', text: ingredients)
    expect(page).to have_css('h3', text: 'Como Preparar')
    expect(page).to have_css('li', text: method)
  end

  scenario 'and validates all fields' do
    cuisine = Cuisine.create(name: 'Árabe')
    recipe_type = RecipeType.create(name: 'Entrada')

    visit new_recipe_path
    click_on 'Enviar'

    expect(page).to have_content('Você deve preencher todos os dados da receita')
  end
end
