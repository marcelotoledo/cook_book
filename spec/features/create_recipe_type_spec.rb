require 'rails_helper'

feature 'User register recipe_type' do
  scenario 'accesses the page correctly' do
    visit root_path
    click_on 'Cadastrar tipo de receita'

    expect(current_path).to eq new_recipe_type_path
    expect(page).to have_css('h1', text: 'Cadastro de tipo de receita')
    expect(page).to have_content('Voltar')
  end

  scenario "and can't be blank" do
    visit new_recipe_type_path
    fill_in 'Nome', with: ''
    click_on 'Enviar'

    expect(page).to have_content('Você deve preencher o nome do tipo de receita')
  end

  scenario 'successfully' do
    visit new_recipe_type_path
    fill_in 'Nome', with: 'Sobremesa'
    click_on 'Enviar'

    expect(page).to have_css('h1', text: 'Sobremesa')
    expect(page).to have_content('Nenhuma receita encontrada para este tipo de receita')
  end
end
