require 'rails_helper'

feature 'User cant duplicate cuisines' do
  scenario 'successfully validation on create' do
    cuisine = Cuisine.create(name: 'Francesa')

    visit new_cuisine_path
    fill_in 'Nome', with: cuisine.name
    click_on 'Enviar'

    expect(page).to have_content('Essa cozinha já existe')
  end

  scenario 'successfully validation on edit' do
    cuisine = Cuisine.create(name: 'Francesa')
    another_cuisine = Cuisine.create(name: 'Italiana')

    visit edit_cuisine_path cuisine
    fill_in 'Nome', with: another_cuisine.name
    click_on 'Editar'

    expect(page).to have_content('Essa cozinha já existe')
  end
end
