require 'rails_helper'

feature 'User cant edit cuisines' do
  scenario 'accesses the page correctly' do
    cuisine = Cuisine.create(name: 'Francesa')

    visit root_path
    click_on cuisine.name
    click_on 'Editar'

    expect(page).to have_css('h1', text: 'Edição de cozinha')
    expect(page).to have_content('Voltar')
  end

  scenario 'successfully' do
    cuisine = Cuisine.create(name: 'Francesa')

    visit edit_cuisine_path cuisine
    fill_in 'Nome', with: 'Italiana'
    click_on 'Editar'

    expect(current_path).to eq cuisine_path(1)
    expect(page).to have_css('h1', text: 'Italiana')
  end

  scenario 'cant be blank' do
    cuisine = Cuisine.create(name: 'Francesa')

    visit edit_cuisine_path cuisine
    fill_in 'Nome', with: ''
    click_on 'Editar'

    expect(page).to have_content('Você deve preencher o nome da cozinha')
  end
end
