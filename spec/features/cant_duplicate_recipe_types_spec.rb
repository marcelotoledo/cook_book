require 'rails_helper'

feature 'User cant duplicate recipe types' do
  scenario 'successfully validation on create' do
    recipe_type = RecipeType.create(name: 'Sobremesa')

    visit new_recipe_type_path
    fill_in 'Nome', with: recipe_type.name
    click_on 'Enviar'

    expect(page).to have_content('Esse tipo de receita já existe')
  end

  scenario 'successfully validation on edit' do
    recipe_type = RecipeType.create(name: 'Sobremesa')
    another_recipe_type = RecipeType.create(name: 'Prato de Entrada')

    visit edit_recipe_type_path recipe_type
    fill_in 'Nome', with: another_recipe_type.name
    click_on 'Editar'

    expect(page).to have_content('Esse tipo de receita já existe')
  end
end
