FactoryBot.define do
  factory :recipe do
    title 'File mignon à parmegiana'
    recipe_type
    cuisine
    difficulty 'Média'
    cook_time 45
    ingredients 'File mignon, alho, cebola, queijo prato, molho de tomate'
    add_attribute(:method) { 'Corte o file em bifes, tempere, frite e mergulhe no molho, sirva com queijo em cima' }
  end
end
