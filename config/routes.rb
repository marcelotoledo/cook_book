Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'home#index'
  resources :cuisines, only: [:create, :edit, :new, :show, :update]
  resources :recipes, only: [:create, :edit, :new, :show, :update]
  resources :recipe_types, only: [:create, :edit, :new, :show, :update]
end
