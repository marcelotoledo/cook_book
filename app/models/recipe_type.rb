class RecipeType < ApplicationRecord
  has_many :recipes

  validates :name, presence: { message: 'Você deve preencher o nome do tipo de receita' }
  validates :name, uniqueness: { message: 'Esse tipo de receita já existe' }
end
