# frozen_string_literal: true

class RecipeTypesController < ApplicationController
  def create
    @recipe_type = RecipeType.new(create_params)

    if @recipe_type.save
      redirect_to recipe_type_path @recipe_type
    else
      @recipe_type
      render :new
    end
  end

  def edit
    @recipe_type = RecipeType.find_by(id: params[:id])
  end

  def new
    @recipe_type = RecipeType.new
  end

  def show
    @recipe_type = RecipeType.find params[:id]

    if @recipe_type.recipes.empty?
      msg = 'Nenhuma receita encontrada para este tipo de receita'
      flash[:notice] = msg
    end
  end

  def update
    @recipe_type = RecipeType.find_by(id: params[:id])

    if @recipe_type.update(create_params)
      redirect_to recipe_type_path @recipe_type
    else
      @recipe_type
      render :edit
    end
  end

  private

  def create_params
    params.require(:recipe_type).permit(:name)
  end
end
