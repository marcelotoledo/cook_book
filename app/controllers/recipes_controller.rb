class RecipesController < ApplicationController
  def create
    @recipe = Recipe.new create_params

    if @recipe.save
      redirect_to recipe_path @recipe
    else
      @recipe
      @cuisines = Cuisine.all
      @recipe_types = RecipeType.all
      render :new
    end
  end

  def edit
    @recipe = Recipe.find params[:id]
    @cuisines = Cuisine.all
    @recipe_types = RecipeType.all
  end

  def new
    @recipe = Recipe.new
    @cuisines = Cuisine.all
    @recipe_types = RecipeType.all
  end

  def show
    @recipe = Recipe.find params[:id]
  end

  def update
    @recipe = Recipe.find_by(id: params[:id])
    if @recipe.update(create_params)
      redirect_to recipe_path @recipe
    else
      @recipe
      @cuisines = Cuisine.all
      @recipe_types = RecipeType.all

      render :edit
    end
  end

  private
    # Using a private method to encapsulate the permissible parameters
    # is just a good pattern since you'll be able to reuse the same
    # permit list between create and update. Also, you can specialize
    # this method with per-user checking of permissible attributes.
    def create_params
      params.require(:recipe).permit(:title, :recipe_type_id, :cuisine_id,
        :difficulty, :cook_time, :ingredients, :method)
    end
end
