class CuisinesController < ApplicationController
  def create
    @cuisine = Cuisine.new create_params

    if @cuisine.save
      redirect_to cuisine_path @cuisine
    else
      @cuisine
      render :new
    end
  end

  def edit
    @cuisine = Cuisine.find_by(id: params[:id])
  end

  def new
    @cuisine = Cuisine.new
  end

  def show
    @cuisine = Cuisine.find params[:id]

    unless @cuisine.recipes.any?
      msg = 'Nenhuma receita encontrada para este tipo de cozinha'
      flash[:notice] = msg
    end
  end

  def update
    @cuisine = Cuisine.find_by(id: params[:id])
    if @cuisine.update(create_params)
      redirect_to cuisine_path @cuisine
    else
      @cuisines
      render :edit
    end
  end

  private

  def create_params
    params.require(:cuisine).permit(:name)
  end
end
